# OnlyOffice

#### 介绍
公司业务需要在线编辑WORD及相关Office文档找了一圈只有这个了其他都收费的,只是简单实现了在线编辑及保存

#### 软件架构
软件架构说明


#### 安装教程

1.  安装教程参考:1.安装docker 2.安装onlyoffice镜像:**sudo docker pull onlyoffice/documentserver** 3.启动
2.  docker启动命令:  **docker run -i -t -d -p 6831:80 --restart=always -v /app/onlyoffice/DocumentServer/logs:/var/log/onlyoffice -v /app/onlyoffice/DocumentServer/data:/var/www/onlyoffice/Data -v /app/onlyoffice/DocumentServer/lib:/var/lib/onlyoffice -v /app/onlyoffice/DocumentServer/db:/var/lib/postgresql onlyoffice/documentserver** 
3.  详细参数使用情况请看ONLYOFFICE文档服务器使用介绍.md

#### 使用说明

1.  docer启动后就可以使用index页面查看编辑效果了页面有一个word文档与excel表格一样一个链接
2.  然后index里的保存回调服务必须是你onlyoffice服务能访问到的地址!!!我回调服务放到文档服务器上跑的所以没问题,我看很多朋友回调服务在本地文档服务在服务器就需要做路由器映射了
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
